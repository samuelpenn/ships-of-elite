# Ships of Elite

Very unofficial Mongoose Traveller 2e version of ships from Elite 1984,
by David Braben and Ian Bell.

These are not direct conversions - there are siginificant differences
between what the computer game allowed, and what the Traveller rules
allow. The main difference is small craft have a limited number of
firmpoints, so can't be equipped with both lasers and missiles.

So the priority has been to make the designs correct for Traveller,
and inspired by the original ships.

See here for the final products:
https://www.notasnark.net/traveller/elite/index

Original plan outlined here:
https://blog.notasnark.net/2023/04/ships-of-elite.html


## Upcoming Designs

  * Constrictor - 400t with lots of armour.

