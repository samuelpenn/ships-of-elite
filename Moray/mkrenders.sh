#!/bin/bash

ship=$1

blender -b ${ship}.blend --python set_camera.py -o //${ship}_front -f 1 -- Front
blender -b ${ship}.blend --python set_camera.py -o //${ship}_top -f 1 -- Top
blender -b ${ship}.blend --python set_camera.py -o //${ship}_side -f 1 -- Side

blender -b ${ship}.blend --python set_camera.py -o //${ship} -f 1 -- Main


for f in *0001.png
do
    mv $f $(echo $f | sed 's/0001//g')
done
