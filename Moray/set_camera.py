import bpy
import sys

argv = sys.argv
argv = argv[argv.index("--") + 1:]

camera="Camera " + argv[0]

def set_background(factor):
    bpy.data.worlds["World"].node_tree.nodes["Value"].outputs[0].default_value = factor

bpy.context.scene.camera = bpy.data.objects[camera]

#for scene in bpy.data.scenes:
#  scene.render.resolution_x = 2048
#  scene.render.resolution_y = 2048

#set_background(0.3)

# Cloud Seed
#bpy.data.materials["Atmosphere"].node_tree.nodes["Math"].inputs[0].default_value = 2 + seed

# Cloud Thickness
#bpy.data.materials["Atmosphere"].node_tree.nodes["Math.003"].inputs[1].default_value = thickness

